from sqlalchemy import Column, Integer, BigInteger, String, ForeignKey
from sqlalchemy.orm import relationship
from database import Base


class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    email = Column(String(75), unique=True)
    password = Column(String(128), nullable=False)

    airwaybillnumbers = relationship("AirwaybillNumber")

    def __init__(self, email, password):
        self.password = password
        self.email = email

    def __repr__(self):
        return '<User %r>' % (self.email)


class AirwaybillNumber(Base):
    __tablename__ = 'airwaybillnumbers'
    id = Column(Integer, primary_key=True)
    airwaybillnumber = Column(BigInteger, unique=True)
    user_id = Column(Integer, ForeignKey('users.id'))

    def __init__(self, airwaybillnumber, user_id):
        self.airwaybillnumber = airwaybillnumber
        self.user_id = user_id

    def __repr__(self):
        return '<AirwaybillNumber %r>' % (self.airwaybillnumber)
