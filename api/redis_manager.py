import os
import yaml
import redis

PROJECT_PATH = os.path.abspath(os.path.dirname(__file__))

with open(os.path.join(PROJECT_PATH, 'config.yml'), 'r') as config_file:
    config = yaml.load(config_file)

r = redis.StrictRedis(
    host=config['production']['database']['redis-host'], 
    port=config['production']['database']['redis-port'], 
    db=0)


def set_key(key, value):
    """
    store key value pair in redis
    """
    r.set(key, value)
    return True


def get_key(key):
    """
    return the value for requested key
    """
    value = r.get(key)
    return value if value else config['constants']['default-awb']


def update_awbs_list(awbs):
    """
    add airwaybill numbers to airwaybills list
    """
    for awb in awbs:
        r.rpush("airwaybill_numbers", awb)
    return True
