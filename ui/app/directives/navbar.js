'use strict';

angular.module('AwbApp')
  .directive('navBar', function() {

    return {
      templateUrl: "/views/navbar.html",
      restrict: 'E',
      scope: {
      },
      link: function postLink(scope, element, attrs) {

      }
    };

  });
