'use strict';

angular
  .module('AwbApp', [
    'ngRoute', 
    'ngCookies',
    'ngAnimate',    
    'toaster',
    'ui.bootstrap'
])

.config(['$routeProvider', 
  function ($routeProvider) {

    $routeProvider
      .when('/login', {
        controller: 'LoginController',
        templateUrl: '/views/login.html'
      })

      .when('/airwaybills', {
        templateUrl: '/views/airwaybills.html',
        controller: 'MainController',
        controllerAs: 'main'
      })

      .when('/airwaybills/generate', {
        templateUrl: '/views/generate.html',
        controller: 'MainController',
        controllerAs: 'main'
      })

      .when('/airwaybills/validate', {
        templateUrl: '/views/validate.html',
        controller: 'MainController',
        controllerAs: 'main'
      })   

      .when('/', {
        redirectTo: '/airwaybills'
      })   

      .otherwise({
        redirectTo: '/login'
      });

  }])

.run(['$rootScope', '$location', '$cookieStore', '$http',
  function ($rootScope, $location, $cookieStore, $http) {
    // keep user logged in after page refresh
    $rootScope.globals = $cookieStore.get('globals') || {};
    if ($rootScope.globals.currentUser) {
        $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.token; // jshint ignore:line
    }

    $rootScope.$on('$locationChangeStart', function (event, next, current) {
        // redirect to login page if not logged in
        if ($location.path() !== '/login' && !$rootScope.globals.currentUser) {
            $location.path('/login');
        }
    });
  }]);
