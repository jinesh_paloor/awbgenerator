'use strict';
 
angular.module('AwbApp')
 
.factory('AuthenticationService',
    ['$http', '$cookieStore', '$rootScope',
    function ($http, $cookieStore, $rootScope) {
        var service = {};

        service.Login = function (email, password, callback) {
            var payload = btoa("email=" + email + "&password=" + password);
            $http.post('/authenticate?'+payload)
               .success(function (response) {
                   callback(response);
               });
        };
 
        service.SetCredentials = function (email, password, token) {
            var authdata = btoa(email + ':' + password);
 
            $rootScope.globals = {
                currentUser: {
                    email: email,
                    authdata: authdata,
                    token: token
                }
            };
 
            $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata; // jshint ignore:line
            $cookieStore.put('globals', $rootScope.globals);
        };
 
        service.ClearCredentials = function () {
            $rootScope.globals = {};
            $cookieStore.remove('globals');
            $http.defaults.headers.common.Authorization = 'Basic ';
        };
 
        return service;
    }]);
